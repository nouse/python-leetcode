from word_search import Solution

def test_answer():
    board = [['a','a']]
    s = Solution()
    assert s.exist(board, 'aa')
    board = [
    ['A','B','C','E'],
    ['S','F','C','S'],
    ['A','D','E','E']
    ]
    s = Solution()
    assert s.exist(board, 'ABCCED')
    assert s.exist(board, 'SEE')
    assert not s.exist(board, 'ABCB')

def test_benchmark(benchmark):
    board = [
    ['A','B','C','E'],
    ['S','F','C','S'],
    ['A','D','E','E']
    ]
    s = Solution()
    benchmark(s.exist, board, 'ESCCBF')
