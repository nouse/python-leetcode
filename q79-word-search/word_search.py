def near(p1, p2):
    return (p1[0] == p2[0] and abs(p1[1]-p2[1]) == 1) or (p1[1] == p2[1] and abs(p1[0] - p2[0]) == 1)

def findSolutions(path, d, character, word):
    for p in d[character]:
        if len(path) > 0 and not near(path[-1], p):
            continue
        if p in path:
            continue

        if len(path) == len(word) -1:
            return True
        path.append(p)
        if findSolutions(path, d, word[len(path)], word):
            return True
        path.pop()
    return False


class Solution:
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        word = word.upper()
        if len(board) == 1 and len(board[0]) == 1:
            return word == board[0][0].upper()
        
        d = dict()

        for i in range(len(board)):
            for j in range(len(board[0])):
                c = board[i][j].upper()
                if c not in d:
                    d[c] = []
                d[c].append((i, j))
        if word[0] not in d:
            return False
        return findSolutions([], d, word[0], word)
