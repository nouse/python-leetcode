from skyline import Solution


def test_answer():
    s = Solution()
    assert s.getSkyline([[2, 4, 10], [3, 7, 15]]) == [[2, 10], [3, 15], [7, 0]]
    assert s.getSkyline([[2, 3, 10], [4, 7, 15]]) == [
        [2, 10], [3, 0], [4, 15], [7, 0]]
    assert s.getSkyline([[2, 7, 15], [3, 4, 10]]) == [[2, 15], [7, 0]]
    assert s.getSkyline([[2, 9, 10], [3, 7, 15], [5, 12, 12]]) == [
        [2, 10], [3, 15], [7, 12], [12, 0]]
    assert s.getSkyline([[1, 2, 1], [1, 2, 2], [1, 2, 3]]) == [[1, 3], [2, 0]]


def test_overlapped():
    s = Solution()
    assert s.getSkyline([[2, 4, 10], [3, 10, 7], [7, 15, 15]]) == [
        [2, 10], [4, 7], [7, 15], [15, 0]]


def test_complex():
    s = Solution()
    assert s.getSkyline([[2, 9, 10], [3, 7, 15], [5, 12, 12], [15, 20, 10], [19, 24, 8]]) == [
        [2, 10], [3, 15], [7, 12], [12, 0], [15, 10], [20, 8], [24, 0]]
