from bisect import bisect, insort


def collect(stack, start, height_of_stack):
    tail = []
    for i in range(start, -1, -1):
        height = stack[i][1]
        if height > height_of_stack:
            if len(tail) > 0 and tail[-1][0] == stack[i][0]:
                tail[-1][1] = height_of_stack
            else:
                tail.append([stack[i][0], height_of_stack])
            height_of_stack = height
    return tail


class Solution:
    def getSkyline(self, buildings):
        """
        :type buildings: List[List[int]]
        :rtype: List[List[int]]
        """
        result = []
        stack = []
        height_of_stack = 0
        for building in buildings:
            if len(stack) == 0:
                height = building[2]
                if height_of_stack < height:
                    if len(result) > 0 and result[-1][0] == building[0]:
                        result[-1][1] = height
                    else:
                        result.append([building[0], height])
                    height_of_stack = height

                insort(stack, (building[1], height))
                continue

            index = bisect(stack, (building[0], 0))

            # recalculate highest
            if index != 0:

                # recalculate height of stack
                height_of_stack = 0
                for i in range(index, len(stack)):
                    if stack[i][1] > height_of_stack:
                        height_of_stack = stack[i][1]

                # pop out of stack
                tail = collect(stack, index-1, height_of_stack)
                result.extend(reversed(tail))

                stack[0:index] = []

            height = building[2]

            if height_of_stack < height:
                if len(result) > 0 and result[-1][0] == building[0]:
                    result[-1][1] = height
                else:
                    result.append([building[0], height])
                height_of_stack = height

            insort(stack, (building[1], height))

        tail = collect(stack, len(stack)-1, 0)

        result.extend(reversed(tail))
        return result
