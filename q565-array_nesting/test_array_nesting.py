from array_nesting import Solution

def test_answer():
    s = Solution()
    assert s.arrayNesting([5,4,0,3,1,6,2]) == 4
    assert s.arrayNesting([0, 1, 2]) == 1
    assert s.arrayNesting([1, 2,3,4,5,0]) == 6
