class Solution:
    def arrayNesting(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        calculated = dict()
        traversed = dict()
        longest = 1
        
        # use DFS, record traversed
        for i in range(len(nums)):
            if i in calculated:
                continue
                
            num = nums[i]
            if i == num:
                calculated[i] = 1
                continue
            
            path_len = 1 #local
            if num in calculated:
                calculated[i] = calculated[num]
                path_len = 1 + calculated[num]
                calculated[i] = path_len
                if path_len > longest:
                    longest = path_len
            
            traversed = {i: 0, num: 1}
            
            while nums[num] not in traversed:
                if nums[num] in calculated:
                    base = calculated[nums[num]]
                    for k, v in traversed.items():
                        path_len = len(traversed)-v+base
                        if path_len > longest:
                            longest = path_len
                    break
                else:
                    traversed[nums[num]] = len(traversed)
                    num = nums[num]
            else:
                base = traversed[nums[num]]
                loop_length = len(traversed) - base
                for k, v in traversed.items():
                    if v < base:
                        calculated[k] = len(traversed) - v
                    else:
                        calculated[k] = loop_length
                path_len = len(traversed)
                if path_len > longest:
                    longest = path_len
            
        return longest
