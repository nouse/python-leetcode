from reordered import Solution

def test_answer():
    s = Solution()
    assert not s.reorderedPowerOf2(123)
    assert s.reorderedPowerOf2(125)
    assert s.reorderedPowerOf2(4120)
    assert not s.reorderedPowerOf2(126)
    assert s.reorderedPowerOf2(679213508)
