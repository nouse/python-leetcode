from bisect import bisect, insort

def chars(N):
    remain, result = divmod(N, 10)
    ch = [result]
    while remain > 0:
        remain, result = divmod(remain, 10)
        insort(ch, result)
    return ch


class Solution:

    charsmap = [[] for i in range(9)]
    for i in range(1, 30):
        ch = chars(1<<i)
        insort(charsmap[len(ch)-1], ch)

    def reorderedPowerOf2(self, N):
        """
        :type N: int
        :rtype: bool
        """
        ch = chars(N)
        index = bisect(self.charsmap[len(ch)-1], ch)
        if index == 0:
            return False
        return self.charsmap[len(ch)-1][index-1] == ch

