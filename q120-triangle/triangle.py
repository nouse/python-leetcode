class Solution:
    def minimumTotal(self, triangle):
        """
        :type triangle: List[List[int]]
        :rtype: int
        """
        if len(triangle) == 1:
            return triangle[0][0]
        
        l = triangle[0]*len(triangle)
        layer_min = l.copy()

        for i in range(1, len(triangle)-1):
            for j in range(i+1):
                l[j] = layer_min[j] + triangle[i][j]

            layer_min[0] = l[0]
            layer_min[i+1] = l[i]
            for j in range(1, i+1):
                layer_min[j] = min(l[j],l[j-1])

        i = len(triangle)-1
        m = layer_min[0]+triangle[i][0]
        for j in range(1, i+1):
            s = layer_min[j] + triangle[i][j]
            if s < m:
                m = s
        return m
