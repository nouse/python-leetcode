from search_132_pattern import Solution


def test_answer():
    s = Solution()
    assert not s.find132pattern([1, 2, 3, 4])
    assert s.find132pattern([3, 1, 4, 2])
    assert not s.find132pattern([1, 0, 1, -4, -3])
    assert not s.find132pattern([0, 1, -4, -3, 2, -5])
    assert s.find132pattern([0, 1, -4, -2, -3])
