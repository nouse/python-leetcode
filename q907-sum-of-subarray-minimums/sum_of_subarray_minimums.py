from bisect import bisect

def collect(l, index):
    left = 1
    right = 1
    sum = 0
    for i in range(len(l)-1, index-1, -1):
        last = l[i]
        left += last[1]
        sum += last[0] * last[1] * right
        right += last[1]

    return (sum, left)


class Solution:
    def sumSubarrayMins(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        if len(A) == 1:
            return A[0]

        l = [(A[0], 1)]
        sum = 0
        num = 0
        for i in range(1, len(A)):
            num = A[i]
            if num > l[-1][0]:
                l.append((num, 1))
                continue

            index = bisect(l, (num, 0))
            sum2, left = collect(l, index)
            sum += sum2
            l[index:] = []
            l.append((num, left))

        return (sum+collect(l, 0)[0]) % 1000000007
