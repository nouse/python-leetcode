from super_ugly_number import Solution

def test_answer():
    s = Solution()
    assert s.nthSuperUglyNumber(12, [2,7,13,19]) == 32
