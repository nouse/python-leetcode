from heapq import heappop, heappush


class Solution:
    def nthSuperUglyNumber(self, n, primes):
        """
        :type n: int
        :type primes: List[int]
        :rtype: int
        """
        if n == 1:
            return 1
        primes.sort()
        candidates = []
        indices = [] # tuple of (value, candidates index)
        for i in range(len(primes)):
            prime = primes[i]
            candidates.append([])
            heappush(indices, (prime, i))

        # if current prime is from candidates[k], then only calculate
        # current*prime[k], current*prime[k+1]..., current*prime[-1]
        i = 1
        while i < n:
            index = heappop(indices)
            current = index[0]
            for j in range(index[1], len(primes)):
                candidates[j].append(primes[j]*current)
            prime = candidates[index[1]].pop(0)
            heappush(indices, (prime, index[1]))
            i += 1
        return current
